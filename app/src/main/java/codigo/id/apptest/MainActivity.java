package codigo.id.apptest;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;

import id.codigo.qnocklib.config.Config;
import id.codigo.qnocklib.controller.Qnock;
import id.codigo.qnocklib.interfaces.SubscribeInterface;
import id.codigo.qnocklib.interfaces.TokenInterface;
import id.codigo.qnocklib.model.TokenGenerate;
import id.codigo.qnocklib.services.TokenServices;
import id.codigo.seedroid.helper.PreferenceHelper;
import id.codigo.seedroid.service.ServiceListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //        String token = FirebaseInstanceId.getInstance().getToken();
//
//        Log.i("Token", token);
//
//        TokenServices tokenServices= new TokenServices();
//        tokenServices.tokenGenerate(new ServiceListener <TokenGenerate> () {
//            @Override
//            public void onSuccess(TokenGenerate tokenGenerate) {
//                Log.i("String", tokenGenerate.toString());
//            }
//
//            @Override
//            public void onFailed(String s) {
//                Log.i("Test", s);
//            }
//        }, "e3zD1hvVJ0Q:APA91bERfjQCAowF_1kAI7tFW-Ttp0LIcLXAFyEU69TE-mMSBdnamEPZpmhF7Kf6mUPvTbP7CwUoeAnU2B4ND7uMfHwQ0NgWq5HiUuVUnCYBZxK6qEC0Lkbvh7lOvzI431EMglcVjYoB", "Berita olimpiyade 2016", "Berita olimpiyade 2016", "Berita olimpiyade 2016", "bx824BJqw3", "#ghjmSDV", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImNvZGlnbyIsImV4cCI6MTQ3MzI0MDkwMDkzOSwiaWF0IjoxNDczMTU0NTAwfQ.lHMZ747nOUQdx1j_m-Kz8v2hOamoLlriEmMZk_ECM4U");
//
//        FirebaseMessaging.getInstance().subscribeToTopic("suram");
//        if (getIntent().getExtras() != null) {
//            for (String key : getIntent().getExtras().keySet()) {
//                Object value = getIntent().getExtras().get(key);
//                Log.d("intent extra", "Key: " + key + " Value: " + value);
//            }
//        }

    String username = "1";
    String appSecret = "lfxHMYUJYJ";
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public static TextView textViewFirebaseId;
    public static TextView textViewToken;

    EditText editTextChannel, editTextChannelUn;
    Button btnSubscribe, btnUnsubscibe;
    ProgressBar progressBar1, progressBar2;

    List<String> channel;
    List<String> channelUn;
    ProgressDialog pd;

    int envi = 1;

    String SERVER_ERROR = "Server error";


//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
////        textViewFirebaseId = (TextView) findViewById(R.id.text_view_firebase_id);
////        textViewToken = (TextView) findViewById(R.id.text_view_token);
////        editTextChannel = (EditText) findViewById(R.id.edit_text_channel);
////        btnSubscribe = (Button) findViewById(R.id.btn_subscribe);
////        editTextChannelUn = (EditText) findViewById(R.id.edit_text_channel_un);
////        btnUnsubscibe = (Button) findViewById(R.id.btn_unsubscribe);
////        progressBar1 = (ProgressBar) findViewById(R.id.progress_bar_1);
////        progressBar2 = (ProgressBar) findViewById(R.id.progress_bar_2);
//
//        btnSubscribe.setOnClickListener(this);
//        btnUnsubscibe.setOnClickListener(this);
//
//        setTitle("Qnock Development");
//
//        //initialize qnock
////        Qnock.init(MainActivity.this, username, appSecret, Qnock.DEV);
////        Qnock.instance.setColor("#3d61ad");
//
//        firebaseRegId = FirebaseInstanceId.getInstance().getToken();
//        textViewFirebaseId.setText(firebaseRegId);
//
//        if (textViewFirebaseId.getText().length() != 0)
//            progressBar1.setVisibility(View.GONE);
//        if (textViewToken.getText().length() != 0)
//            progressBar2.setVisibility(View.GONE);
//
//        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
//                    String data = intent.getStringExtra("dataPush");
//                    String message = intent.getStringExtra("messagePush");
//                    String unixId = intent.getStringExtra("unixIdPush");
//
//                    Log.e("PAYLOAD DATA", "UNIX_ID : " + unixId);
//                    Log.e("PAYLOAD DATA", "MESSAGE : " + message);
//                    Log.e("PAYLOAD DATA", "DATA : " + data);
//
//                    Toast.makeText(getApplicationContext(), "Push notification received", Toast.LENGTH_LONG).show();
//                }
//            }
//        };
//
//        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (intent.hasExtra("token")) {
//                    progressBar2.setVisibility(View.GONE);
//                    String message = intent.getStringExtra("token");
//                    textViewToken.setText(message);
//                    if (pd != null) {
//                        if (pd.isShowing()) {
//                            Toast.makeText(MainActivity.this, "Token has been updated", Toast.LENGTH_SHORT).show();
//                            pd.dismiss();
//                        }
//                    }
//                } else {
//                    progressBar2.setVisibility(View.GONE);
//                    textViewToken.setText(SERVER_ERROR);
//
//                }
//                if (intent.hasExtra("firebaseId")) {
//                    progressBar1.setVisibility(View.GONE);
//                    textViewFirebaseId.setText(intent.getStringExtra("firebaseId"));
//                }
//            }
//        }, new IntentFilter(Config.QNOCK_TOKEN));
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
//                new IntentFilter(Config.PUSH_NOTIFICATION));
//
//        NotificationUtils.clearNotifications(getApplicationContext());
//    }
//
//    @Override
//    protected void onPause() {
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
//        super.onPause();
//    }

    String firebaseRegId;

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        super.onCreateOptionsMenu(menu);
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_env, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        super.onOptionsItemSelected(item);
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
//        if (item.getItemId() == R.id.action_dev) {
//            envi = 1;
//            setup(1);
//        } else if (item.getItemId() == R.id.action_stag) {
//            envi = 2;
//            setup(2);
//        } else if (item.getItemId() == R.id.action_production) {
//            envi = 3;
//            setup(3);
//        } else if (item.getItemId() == R.id.action_refresh) {
//            progressBar2.setVisibility(View.GONE);
//            textViewToken.setText("");
//            editTextChannel.setText("");
//            editTextChannelUn.setText("");
//            setup(envi);
//        }
//        return true;
//    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Qnock qnock = new Qnock();

//        qnock.subscribeChannel(new SubscribeInterface() {
//            @Override
//            public void onSubscribeGet(boolean status, String msg, String response) {
//
//            }
//        }, userId, channel, device, token);
        qnock.getToken(new TokenInterface() {
            @Override
            public void onTokenGet(boolean status, String msg, String token) {
                Log.d("ontokenset", msg+"");
            }
        }, username, appSecret);
//        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//            qnock.getStatus(new StatusInterface() {
//                @Override
//                public void onStatusGet(boolean status, String msg, String response) {
//
//                }
//            })
//            }
//        });
    }

    private void setup(int type) {
        String env;
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        if (type == 1) {
            env = "Development";
            setTitle("Qnock Development");
            showProgressDialog(env);
            setupQnock(Qnock.DEV);
        } else if (type == 2) {
            env = "Staging";
            setTitle("Qnock Staging");
            showProgressDialog(env);
            setupQnock(Qnock.STG);
        } else if (type == 3) {
            env = "Production";
            setTitle("Qnock Production");
            showProgressDialog(env);
            setupQnock(Qnock.PRD);
        }
    }

    private void setupQnock(int env) {
        clearPrefereces();
        if (textViewFirebaseId.getText().toString().equals("") || textViewFirebaseId.getText().toString().length() == 0)
            FirebaseInstanceId.getInstance().getToken();
//        Qnock.init(MainActivity.this, username, appSecret, env);
//        Qnock.instance.setEnvironment(Qnock.PRD);
//        Qnock.instance.setColor("#3d61ad");

        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("token")) {
                    progressBar2.setVisibility(View.GONE);
                    String message = intent.getStringExtra("token");
                    textViewToken.setText(message);
                    if (pd != null) {
                        if (pd.isShowing()) {
                            Toast.makeText(MainActivity.this, "Token has been updated", Toast.LENGTH_SHORT).show();
                            pd.dismiss();
                        }
                    }
                } else if (!intent.hasExtra("token")) {
                    progressBar2.setVisibility(View.GONE);
                    if (pd.isShowing()) {
                        textViewToken.setText(SERVER_ERROR);
                        pd.dismiss();
                    }
                }
                if (intent.getAction().equals("firebaseId")) {
                    progressBar1.setVisibility(View.GONE);
                    FirebaseMessaging.getInstance().subscribeToTopic("global");
                    textViewFirebaseId.setText(intent.getStringExtra("firebaseId"));
                }
            }
        }, new IntentFilter("id.codigo.qnocklib.TokenReceiver"));
    }

    private void showProgressDialog(String s) {
        pd = new ProgressDialog(MainActivity.this);
        pd.setMessage("Setup Qnock " + s);
        pd.show();
        pd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                progressBar2.setVisibility(View.GONE);
            }
        });

        progressBar2.setVisibility(View.VISIBLE);
        textViewToken.setText("");
    }

    private void clearPrefereces() {
        PreferenceHelper ph = new PreferenceHelper();
        ph.saveSession(Config.tokenDate, "");
        ph.saveSession(Config.token1, "");
    }

    @Override
    public void onClick(View view) {
        if (view == btnSubscribe) {
            subscribe();
        }
        if (view == btnUnsubscibe) {
            unSubscribe();
        }
    }

    private void subscribe() {

        /*if (!textViewFirebaseId.getText().toString().equals("") || textViewFirebaseId.getText().toString().length() != 0 || textViewToken.getText().toString().length() != 0) {
            if (!textViewToken.getText().toString().equals(SERVER_ERROR)) {
                if (editTextChannel.getText().toString().length() != 0 || !editTextChannel.getText().toString().equals("")) {
                    if (editTextChannel.getText().toString().contains(",")) {
                        channel = Arrays.asList(editTextChannel.getText().toString().split("\\s*,\\s*"));
                        Qnock.instance.subscribeChannel(channel, firebaseRegId, "", new GeneralListener<TokenModel>() {
                            @Override
                            public void onSuccess(String var1) {
                                Toast.makeText(MainActivity.this, var1, Toast.LENGTH_SHORT).show();
                                editTextChannel.setText("");
                            }

                            @Override
                            public void onFailed(String var1) {
                                Toast.makeText(MainActivity.this, var1, Toast.LENGTH_SHORT).show();
                                editTextChannel.setText("");
                            }
                        });
                    } else {
                        Qnock.instance.subscribeChannel(editTextChannel.getText().toString(), firebaseRegId, "", new GeneralListener<TokenModel>() {
                            @Override
                            public void onSuccess(String var1) {
                                Toast.makeText(MainActivity.this, var1, Toast.LENGTH_SHORT).show();
                                editTextChannel.setText("");
                            }

                            @Override
                            public void onFailed(String var1) {
                                Toast.makeText(MainActivity.this, var1, Toast.LENGTH_SHORT).show();
                                editTextChannel.setText("");
                            }
                        });
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Channel is empty!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(MainActivity.this, SERVER_ERROR, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(MainActivity.this, "FirebaseId or token is null", Toast.LENGTH_SHORT).show();
        }*/
    }

    private void unSubscribe() {
        /*if (!textViewFirebaseId.getText().toString().equals("") || textViewFirebaseId.getText().toString().length() != 0 || textViewToken.getText().toString().length() != 0) {
            if (!textViewToken.getText().toString().equals(SERVER_ERROR)) {
                if (editTextChannelUn.getText().toString().length() != 0 || !editTextChannelUn.getText().toString().equals("")) {
                    if (editTextChannelUn.getText().toString().contains(",")) {
                        channelUn = Arrays.asList(editTextChannelUn.getText().toString().split("\\s*,\\s*"));
                        Qnock.instance.unsubscribeChannel(channelUn, firebaseRegId, new GeneralListener<TokenModel>() {
                            @Override
                            public void onSuccess(String var1) {
                                Toast.makeText(MainActivity.this, var1, Toast.LENGTH_SHORT).show();
                                editTextChannel.setText("");
                            }

                            @Override
                            public void onFailed(String var1) {
                                Toast.makeText(MainActivity.this, var1, Toast.LENGTH_SHORT).show();
                                editTextChannel.setText("");
                            }
                        });
                    } else {
                        Qnock.instance.unsubscribeChannel(editTextChannelUn.getText().toString(), firebaseRegId, new GeneralListener<TokenModel>() {
                            @Override
                            public void onSuccess(String var1) {
                                Toast.makeText(MainActivity.this, var1, Toast.LENGTH_SHORT).show();
                                editTextChannel.setText("");
                            }

                            @Override
                            public void onFailed(String var1) {
                                Toast.makeText(MainActivity.this, var1, Toast.LENGTH_SHORT).show();
                                editTextChannel.setText("");
                            }
                        });
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Channel is empty!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(MainActivity.this, SERVER_ERROR, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(MainActivity.this, "FirebaseId or token is null", Toast.LENGTH_SHORT).show();
        }*/
    }
}

package codigo.id.apptest;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Codigo on 10/12/2017.
 */

public class QnockMessagingService extends FirebaseMessagingService {
    private static final String TAG = codigo.id.apptest.QnockMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.i(TAG, "got push");
        Log.i(TAG, "From: " + remoteMessage.getFrom());
        Log.i(TAG, "Data: " + remoteMessage.getData());
        Log.i(TAG, "Data SIZE: " + remoteMessage.getData().size());
        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//        }
//        if (remoteMessage == null)
//            return;

//        Qnock.instance.registerReceiver(getApplicationContext(), MainActivity.class);
//
//        //UTK DI BACKGROUND
//        // Check if message contains a data payload.
//        if (remoteMessage.getData().size() > 0) {
//            Qnock.instance.handleData(remoteMessage.getData(), new PushCallback() {
//                @Override
//                public void onNotificationReceiveForeground(Map<String, String> map) {
//                    System.err.println("DATA din " + map.get(QNOCKPUSH.DATA.toString()));
//                    System.err.println("DATA din " + map.get(QNOCKPUSH.MESSAGE.toString()));
//                    System.err.println("DATA din " + map.get(QNOCKPUSH.UNIX_ID.toString()));
//                   /* Intent i = new Intent(getApplicationContext(), TestActivity.class);
//                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    i.putExtra(QNOCKPUSH.DATA.toString(), map.get(QNOCKPUSH.DATA.toString()));
//                    i.putExtra(QNOCKPUSH.MESSAGE.toString(), map.get(QNOCKPUSH.MESSAGE.toString()));
//                    i.putExtra(QNOCKPUSH.UNIX_ID.toString(), map.get(QNOCKPUSH.UNIX_ID.toString()));
//                    startActivity(i);*/
//                }
//
//            });
//            Qnock.instance.handleNotification(remoteMessage.getData().toString());
//        }
//        if (remoteMessage.getData().size() > 0) {
//            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
//
//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob();
//            } else {
//                // Handle message within 10 seconds
//                handleNow();
//            }
//
//        }

        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//        }
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }


    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }
}

package codigo.id.apptest;

import android.util.Base64;
import android.util.Log;

import java.util.HashMap;

import id.codigo.seedroid.SeedroidApplication;
import id.codigo.seedroid.configs.RestConfigs;

/**
 * Created by Codigo on 10/12/2017.
 */

public class AppTestAplication extends SeedroidApplication {
    public AppTestAplication(){
        //RestConfigs.isUsingBasicAuth = true;
        /*String credentials = "1" + ":" + "lfxHMYUJYJ";
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.DEFAULT);
        Log.d("auth",auth);*/
        RestConfigs.defaultHeader.put("Authorization","Basic MTpJZnhITVlVSllK");
        //RestConfigs.defaultHeader.put("Authorization",auth);
        RestConfigs.defaultHeader.put("Content-Type","application/json; charset=utf-8");
        //RestConfigs.rootUrl = "http://push.Qnock.netconnect.stg.codigo.id";
        RestConfigs.rootUrl = "http://push.qnock.netconnect.stg.codigo.id";
    }
}

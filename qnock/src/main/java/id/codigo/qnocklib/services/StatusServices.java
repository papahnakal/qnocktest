package id.codigo.qnocklib.services;

import java.util.HashMap;
import java.util.Map;

import id.codigo.qnocklib.model.GetStatus;
import id.codigo.seedroid.configs.RestConfigs;
import id.codigo.seedroid.helper.HttpHelper;
import id.codigo.seedroid.service.ServiceListener;

/**
 * Created by Codigo on 9/29/2017.
 */

public class StatusServices {
    public void status (ServiceListener <GetStatus> callback, String appStatus, String token){
        String url = RestConfigs.rootUrl + "/apps/status";

        HttpHelper.getInstance().get(url, callback);

        Map<String, String> statsApp = new HashMap<>();
        statsApp.put("app_secret", appStatus);
        statsApp.put("token", token);

        HttpHelper.getInstance().post(url, statsApp, callback);

    }
}

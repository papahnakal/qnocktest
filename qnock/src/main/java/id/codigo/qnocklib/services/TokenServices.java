package id.codigo.qnocklib.services;

import android.net.Credentials;
import android.util.Base64;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import id.codigo.qnocklib.model.TokenGenerate;
import id.codigo.seedroid.configs.RestConfigs;
import id.codigo.seedroid.helper.HttpHelper;
import id.codigo.seedroid.service.ServiceListener;

/**
 * Created by Codigo on 9/28/2017.
 */

public class TokenServices {
    public void tokenGenerate(String userId, String appsecret,ServiceListener <TokenGenerate> callback){
        String url = RestConfigs.rootUrl + "/token";
        Log.d("url",url);
        Map<String, String> token = new HashMap<>();
        token.put("user_token_id", userId);
        token.put("app_secret", appsecret);
        /*token.put("alert","");{
  "user_token_id":["c1Edt51CcG0:APA91bHnNgZK2BW_4cvA-_ZkEk46VtBzlyy1fgbYouyr2BelrJfcpjLLBKIWAtA0p6nNft0pt9Wdwi5ziZVoaiKjk4_H3f_Mn1ZJemm8UycSsReF8VyGjiPyYmnuz_W80Y3GlOiF00-u","f0gO34iauQU:APA91bH5x0f0D-zn3mzhN9JF5TRCRPkHQfvJY_kpGJtlqXQoP__yw_KaJvBMoUwQ60uq4jSgCAYQjCNYjrtoQQomH2G3t7IdjL7WZK9b_t2DQhey8iC0WKFmdOl0NC3codRaTSb00r_A","c1Edt51CcG0:APA91bHnNgZK2BW_4cvA-_ZkEk46VtBzlyy1fgbYouyr2BelrJfcpjLLBKIWAtA0p6nNft0pt9Wdwi5ziZVoaiKjk4_H3f_Mn1ZJemm8UycSsReF8VyGjiPyYmnuz_W80Y3GlOiF00-u"],
  "message":{"body":{"message":"Berita olimpiyade 2016"},"alert":{"title":"Berita olimpiyade 2016","body":"Berita olimpiyade 2016","icon":"ic_stat_name","click_action":"fcm.ACTION.HELLO"}},
  "app_secret":"hxorYmybnk",
  "unix_id":"#ghjmSDV",
  "token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImRzZHMiLCJleHAiOjE0NzM4NTc3NjI5OTksImlhdCI6MTQ3Mzc3MTM2M30.gZWDouGe-cuIDWoZ7ste0Z7pfHGxPocBWttIv9g073E"
}
        token.put("message","Berita olimpiyade 2016");
        token.put("body","Berita olimpiyade 2016");
        token.put("icon","");
        token.put("unix_id","0");
        token.put("click_action","");*/

        HashMap<String,String>header = new HashMap<>();
        header.putAll(RestConfigs.defaultHeader);

        HttpHelper.getInstance().post(url, header, token, callback);
    }



}

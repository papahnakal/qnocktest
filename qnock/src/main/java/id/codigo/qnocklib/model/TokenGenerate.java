package id.codigo.qnocklib.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Codigo on 9/28/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenGenerate extends BaseModel{


    private String display_message;
    private String message;

    public String getDisplay_message() {
        return display_message;
    }

    public void setDisplay_message(String display_message) {
        this.display_message = display_message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

package id.codigo.qnocklib.model;

/**
 * Created by Codigo on 9/28/2017.
 */

public class BaseModel {
    String status;
    String time;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

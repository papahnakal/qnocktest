package id.codigo.qnocklib.interfaces;

import java.util.ArrayList;

import id.codigo.qnocklib.model.UnSubsChannel;

/**
 * Created by Codigo on 10/5/2017.
 */

public interface UnsubscribeInterface {
    void onUnsubscribeGet(boolean status, String msg, String response);
}

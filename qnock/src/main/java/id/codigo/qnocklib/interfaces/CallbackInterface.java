package id.codigo.qnocklib.interfaces;

/**
 * Created by Codigo on 10/5/2017.
 */

public interface CallbackInterface {
    void onCallbackGet(boolean status, String msg, String response);
}

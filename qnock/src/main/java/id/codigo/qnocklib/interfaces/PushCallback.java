package id.codigo.qnocklib.interfaces;

import java.util.Map;

/**
 * Created by Codigo on 10/9/2017.
 */

public interface PushCallback {
    void onNotificationReceiveForeground(Map<String, String> data);

}

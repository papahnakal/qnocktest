package id.codigo.qnocklib.interfaces;

/**
 * Created by Codigo on 10/5/2017.
 */

public interface TokenInterface {
    void onTokenGet(boolean status, String msg, String token);
}
